import {
    LOGIN,
    LOGOUT,
    SET_VALUE_REGISTER
} from "./actionType"

const intializeState = {
    user:{
        username:"",
        isLoggedIn:false

    },
    register:{
        username:"",
        password:"",
        confirmPassword:"",
        email:"",
        phoneNumber:""
    }
}
const AuthReducers = (state = intializeState, action) => {
    switch (action.type) {
        case SET_VALUE_REGISTER:
            return {
                ...state,
                register:{
                    ...state.register,
                }
            }
        default:
            break;
    }
}

export default AuthReducers;