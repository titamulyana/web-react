import './App.css';
import {Route, Routes} from 'react-router-dom'
import MainPages from './pages/MainPage';

function App() {
  return (
    <Routes>
      <Route path='/' element={<MainPages />} />
    </Routes>
  );
}

export default App;
